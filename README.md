# Olá, Seja bem-vindo ao meu Github 🤝


Analista de Cibersegurança e Gestor de Programa e Projeto de TI.
- 💻 Administrador Linux e Desenvolvedor Web Júnior
- 👯 Disponível para colaborar em diversos projetos de tecnologia
- 📫 idaleciosilva.dev@gmail.com

## Redes Sociais
<div style="display: inline_block">
 
  [![Instagram](https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white)](https://www.instagram.com/idaleciosilvatech/)
  [![Linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/idal%C3%A9cio-silva-4048b7148/)
  <img alt=" " src="https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white">
